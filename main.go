package main

import "github.com/webview/webview"

func main() {
	debug := true
	w := webview.New(debug)
	defer w.Destroy()
	w.SetTitle("Cinny")
	w.SetSize(800, 600, webview.HintNone)
	w.Navigate("https://cinny.envs.net/")
	w.Run()
	w.Destroy()
}
